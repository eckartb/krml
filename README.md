# Knowledge Reasoning and Machine Learning

This is the "companion" R package corresponding to the course Knowledge Reasoning and Machine Learning from the Bioinformatics program at Hood College in Frederick, Maryland.

## How to get set up

First, Ensure that the R package `devtools` is installed (otherwise install it with `install.packages("devtools")` or within RStudio: Tools->Install Packages. 
Next, within an interactive R session, issue the command:

`devtools::install_bitbucket("eckartb/krml")`

## Who to contact

Eckart Bindewald <bindewald@hood.edu>
