---
title: "Bioinformatics Data via AnnotationHub"
author: "Eckart Bindewald"
date: "`r Sys.Date()`"
output: rmarkdown::html_vignette
vignette: >
  %\VignetteIndexEntry{"Bioinformatics Data via AnnotationHub"}
  %\VignetteEngine{knitr::rmarkdown}
  %\VignetteEncoding{UTF-8}
---

```{r setup, include = FALSE}
knitr::opts_chunk$set(
  collapse = TRUE,
  c omment = "#>"
)
```

## Challenges of Biological Data

* Large datasets
* Different formats
* Hosted by different institutions
* How to implement simple and uniform access?

## The AnnotationHub

* Large datasets are no problem
* Only metadata and URLs are stored (akin to Google)
* Uniform way to filter, search and download datasets in R

## Installation

```{r}
# install.packages("BiocManager")
# BiocManager::install("AnnotationHub")
# # deprecated: source("https://bioconductor.org/biocLite.R")
# # deprecated: biocLite("AnnotationHub")
library(AnnotationHub)
ah<-AnnotationHub()
```

## Available Types of Data

The type of R datastructure is available with attribute `rdataclass`

```{r}
unique(ah$rdataclass)
```

## Subsetting

One can use the `subset` function to filter by different criteria
```{r}
ah <- subset(ah,rdataclass=="GRanges")
ah <- subset(ah,species=="Homo sapiens")
ah <- subset(ah, genome=="hg19") # very important!
```

## A Browser-Based Search Interface

The function `display` opens web browser to allow for an interactive search form (URL <http://127.0.0.1:4689>)
```
display(ah) # search for "H3K4me3” in browser or R session: query(ah, "H3K4me3")
```
We choose here the example of a dataset with ID `AH46826`.

```{r}
b <- ah["AH46826"]
b
```

We can choose any of the shown features with the "$" operator:

```{r}
b$dataprovider
b$species
b$genome
b$sourceurl
```

The `sourceurl` can be used to download the dataset to your computer!

```{r}
# uncomment to run:
# download.file(b$sourceurl,destfile = "AH46826.gz") # try meaningful name and ending
```

## References

* https://bioconductor.org/packages/release/bioc/html/AnnotationHub.html
* https://kasperdanielhansen.github.io/genbioconductor/html/AnnotationHub.html

## Excercises

* Download the example dataset by uncommenting the `download.files` command in this file
* Create a new AnnotationHub instance. Filter by `genome=="dm3"` (dm3 is the name of the latest version of the __Drosophila melanogaster__ genome). Also filter so that the `sourcetype` attribute has the value "BED" (Browser-Embedded Data, a tabular text format for genomic interval data). Choose a data set from the resulting list. Download the data set using `download.files`. Open the downloaded file using a text editor.
