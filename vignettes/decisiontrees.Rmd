---
title: "Decision Trees"
author: "Eckart Bindewald"
date: "1/19/2019"
output:
  ioslides_presentation:
    smaller: true
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

## Library `C50` and Data

```{r message=FALSE, warning=FALSE}
# install.packages("C50")
library(C50)
library(krdata)
data(k_churn)
```

Even though we issued just one `data` function call, we obtain 4 data structures corresponding to input data and outcomes for testing and training:

```{r}
ls()
```

## A Look at the Data

The outcome to be predicted are found in the last column (20), all other columns are the data:

## Data of Custome "Churn" (i.e. Attrition)

```{r echo=FALSE}
str(k_churn_train_data)
```

## Training the Model: Creating a Decision Tree

```{r}
tree_model <- C5.0(x = k_churn_train_data, y = factor(k_churn_train_out))
summary(tree_model)
```

## Applying Testing the Model

```{r}
churnpred <- predict(tree_model, k_churn_test_data)
```

## Model Performance - Confusion Matrix

```{r warning=FALSE, message=FALSE}
library(knitr) # for function "kable"
kable(table(factor(k_churn_test_out), churnpred))
```

## Boosting

The "Boosting" approach uses several trees simultaneously in order to predict prediction accuracy.

```{r}
tree_model2 <- C5.0(x = k_churn_train_data, y = factor(k_churn_train_out),trials=10)
summary(tree_model2)
```

## Applying Testing the Model

```{r}
churnpred2 <- predict(tree_model2, k_churn_test_data)
```

## Model Performance - Confusion Matrix

```{r warning=FALSE, message=FALSE}
kable(table(factor(k_churn_test_out), churnpred2))
```

## Rule Model

```{r}
rule_model <- C5.0(x = k_churn_train_data, y = factor(k_churn_train_out),rules=TRUE)
summary(rule_model)
```

## Applying Testing the Rule Model

```{r}
churnpred_rules <- predict(rule_model, k_churn_test_data)
```

## Rule Model Performance - Confusion Matrix

```{r warning=FALSE, message=FALSE}
kable(table(factor(k_churn_test_out), churnpred_rules))
```
