---
title: "Naive Bayes Classification"
author: "Eckart Bindewald"
date: "1/18/2019"
fig.width: 4
fig.height: 3
output:
  ioslides_presentation:
      smaller: yes

---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

## Bayesian vs Frequentist Statistics

```{r echo=FALSE}
knitr::include_graphics("frequentists_vs_bayesians.png")
```

From: <https://www.cs.ubc.ca/~murphyk/Bayes/bayesrule.html>

## Models of Decision Making

![The brain compares for which choice the expected outcome is better than the other based on two input signals. From http://people.brandeis.edu/~pmiller/PAPERS/Encyclopedia_of_Computational_Neuroscience_Decision_Making_Models.pdf](twochoices.png)

## Bayes Rule

The probability of two events A and B both occuring is given through the multiplication rule:

$$ P(A \text{ and } B) = P(A|B)P(B)$$
but also
$$ P(A \text{ and } B) = P(B|A)P(A)$$
Therefore:
$$P(A|B)P(B)=P(B|A)P(A)$$

Rearranging this equation one obtains:

$$P(A|B)=\frac{P(B|A)P(A)}{P(B)}$$

## Reinterpreting the Bayes Rule

This equation can be interpretated in an interesting way:

$$P(\text{Hypothesis}|\text{Data})=\frac{P(\text{Data}|\text{Hypothesis})P(\text{Hypothesis})}{P(\text{Data})}$$

In other words, the probability of a hypothesis being true (given the data) is proportional to the probability of observing the data (given a hypothesis).
The term $P(\text{Hypothesis})$ is often called a priori probability of a hypothesis. It corresponds to the estimated chance of a hypothesis being true in the absence of any new data.

Nomenclature:

* Posterior probability: $P(\text{Hypothesis}|\text{Data})$
* Likelihood: $P(\text{Data}|\text{Hypothesis})$
* Prior probability: $P(\text{Hypothesis})$

## Alternative Nomenclature

```{r echo=FALSE}
knitr::include_graphics("Bayes_Theorem.png")
```

From: <https://betterexplained.com/articles/an-intuitive-and-short-explanation-of-bayes-theorem/>

## Frequentist's Objection

A statistician from the classical "frequentist" school would object that the term $P(Hypothesis|Data)$ is meaningless. A hypothesis is a statement about this world. This statement is either right or wrong, one cannot attach a probability to it.

Contentious debates among otherwise harmony-seeking experts! For example, clinical trials are based on classical hypothesis tests and NOT Bayes rule.

However, our brains do not seem to care about our philosophical debates but seem to happily apply whatever works (including reasoning inspired by Bayes rule) (see for example <https://www.nature.com/articles/nature02169> where they show that prior knowledge is used in case of noise visual information)!



## Using Bayes Rule for Machine Learning

Estimating which hypothesis is true is another way of classifying data or feature vectors. Imagine there a n-dimensional feature vectors $(x_1,...,x_n)$ that each correspond to one of $m$ different classes $c_1,c_2,...,c_m$. We want to find out the probability of class $c_i$ for a given feature vector:

$$P(c_i|x_1,...,x_n) \propto P(x_1,...,x_n | c_i) \times P(c_i)$$
The a priori probabilities $P(c_i)$ are suprisingly simple to estimate: they are simply the proportion of the training data the correspond to class $c_i$. But what about the term $P(x_1,...,x_n | c_i)$? Knowing these values corresponds to knowing the $n$-dimensional probability density corresponding to class $c_i$. 

## Assuming Independence

Now we introduce a strong simplification that led to the word "naive".
Remember, if variables that all must have a certain value are independent, the multiplication rule of probability states that the probability of all values having the certain value is the product of the individual probabilities:

$$P(x_1,...,x_n | c_i)=P(x_1|c_i)P(x_2|c_i)...P(x_n|c_i)$$

## The Naive Bayes Equation

$$P(c_i|x_1,...,x_n) \propto P(c_i)\prod_{j=1}^{n}P(x_j|c_i)$$
The proportionality (instead of equality) is not a big problem: all we have to do is to remember normalizing in the sense that the sum of probabilities of all classes must sum up to 1.0 and we can normalize as a last step after all unnormalized scores corresponding to the right-hand side of the equation are computed.

## Setup

We need several R packages for the analysis in this section:

```{r warning=FALSE, message=FALSE}
library(e1071) # for function naiveBayes
library(krml) # companion R package of course
library(krdata) # companion R package with data sets prepared for machine learning
```

## Example: Categorical Data

```{r}
# data(HouseVotes84,package="mlbench")
data(k_housevotes84b,package="krdata")
knitr::kable(head(k_housevotes84b_train_data))
```

## Simplified example of voting data

```{r}
tiny_tr_d <- k_housevotes84b_train_data[1:8,1]
tiny_tr_c <- k_housevotes84b_train_class[1:8]
knitr::kable(cbind(VotedYes=tiny_tr_d,Republican=tiny_tr_c)) # combine data+labels
```

## Tabulation of Votes

Again, we randomly choose 8 members of congress and are tabulating how many voted for or against one particular bill (first bill in 1984):

* Rows: Voted Against (FALSE) vs Voted For (TRUE)
* Columns: Democrat (FALSE) vs Republican (TRUE)

```{r}
tiny_tr <- as.data.frame(cbind(tiny_tr_d,tiny_tr_c))
colnames(tiny_tr) <- c("VotedYes","Republican")
kable(table(tiny_tr$VotedYes,tiny_tr$Republican))
```

In other words, from the 4 randomly chosen Republicans 3 voted against the bill and 1 voted for it. From the 4 randomly chosen Democrats 2 voted for the bill and 2 voted against it.

## Task: Given Voting Outcome Predict Party Affiliation

Given the vote of a certain member of congress, what is the probability that the person belonged to a certain party?

Given the 
$$P(\text{Republican}|\text{VotedYes}) \propto P(\text{Republican})\prod_{j=1}^{n}P(\text{VotedYes}|\text{Republican})$$
The table shows, that of the 4 randomly members of congress that happen to be Republican, 1 voted in favor and 3 voted against the chosen bill in question.
So the term can be estimated as $P(\text{VotedYes}|\text{Republican}) = \frac{1}{4}=0.25$. Also, we estimate the a priory probability of randomly choosing a Republican versus a Democrat as 0.5.
$$P(\text{Republican}|\text{VotedYes}) \propto 0.5 \times 0.25 = 0.125$$

## Applying the Bayes Rule (cont.)

We can repeat this process to compute the unnormalized probability of a member of congress being a Democrat (out of 4 Democrats 2 voted in favor 2 against ):

$$P(\text{Democrat}|\text{VotedYes}) \propto 0.5 \times \frac{2}{4} = 0.25$$

## Normalizing Values obtained by the Bayes Rule

Lastly, we have to normalize so that the sum of probabilities is 1.0: $P(\text{Republican}|\text{VotedYes}) + P(\text{Democrat}|\text{VotedYes}) = 1.0$. The sum of unnormalized probabilities is 0.125 + 0.25 = 0.375, therefore:
$$P(\text{Republican}|\text{VotedYes}) = 0.125 / 0.375 = 0.3333$$
$$P(\text{Democrat}|\text{VotedYes}) = 0.25 / 0.375 = 0.6667$$

## A "Sanity Check"

With all those abstract probabilities it is a good idea to frequently perform "sanity" checks to see if the obtained values make sense:
If we look at all members of congress in out training data that voted in favor of the first bill in 1984, we obtain (FALSE corresponds to Democrate, TRUE to Republican):

```{r}
table(k_housevotes84b_train_class[k_housevotes84b_train_data[,1]==TRUE])
```

In other words the probabilities for the whole training data is

$$P(\text{Republican}|\text{VotedYes})=\frac{18}{62}=0.2903$$
$$P(\text{Democrat}|\text{VotedYes})=\frac{44}{62}=0.7097$$

In other words, even though our initial estimation was based on only 8 randomly chosen members of congress, we obtain roughly similar values compared to utilizing the whole training data set.


## Exercises

1. Compute the probability $P(\text{Republican}|!\text{VotedYes})$, in other words the probability a certain vote being *against* the bill, that the caster of the vote is Republican.

2. Repeat the calculation for estimating $P(\text{Republican}|\text{VotedYes})$ but assume that the a priori probability of randomly choosing a Republican among the voters is not 0.5 but $P(\text{Republican})=0.9$. How is the prediction of the party affiliation for a given vote affected? 

## Exercise 1 - Solution

Solution: We need to compute all unnormalized scores:

$$P(\text{Republican}|!\text{VotedYes}) \propto P(\text{Republican})\prod_{j=1}^{n}P(!\text{VotedYes}|\text{Republican})$$
$$=0.5 \times \frac{3}{4} = 0.375$$

$$P(\text{Democrat}|!\text{VotedYes}) \propto P(\text{Democrat})\prod_{j=1}^{n}P(!\text{VotedYes}|\text{Democrat})$$
$$=0.5 \times \frac{2}{4} = 0.25$$
The normlization factor is $0.375+0.25=0.625$ leading to:

$$P(\text{Republican}|!\text{VotedYes}) = 0.375/0.625=0.6$$
$$ P(\text{Democrat}|!\text{VotedYes})=0.25/0.625=0.4$$

## Help from Computers

Bayesian reasoning is fascinating and rewarding - but the actual computations quickly become cumbersome. Fortunately we have help from the computer!

The R package `e1071` provides the function `naiveBayes` that essentially performs the computations we performed manually.

Note: do not mind the strange name `e1071` - it is due to an internal naming of the research group of the Technical University Wien (Vienna) / Austria.

## `naiveBayes` - Tiny Example

```{r}
tiny_tr_f <- factor(tiny_tr_c)
tiny_tr_df <- as.data.frame(tiny_tr_d)
tinymodel <- naiveBayes(tiny_tr_df,tiny_tr_c)
print(tinymodel)
```

## `naiveBayes` - continued
```{r}
pred1 <- predict(tinymodel,tiny_tr_df) # not good - reused training for testing
kable(data.frame(cbind(Republican=tiny_tr_c,Predicted=pred1,Correct=(tiny_tr_c==pred1))))
```

## `naiveBayes` - Larger Example - Training The Model

```{r}
model <- naiveBayes(k_housevotes84_train_data,k_housevotes84_train_class)
pred <- predict(model, k_housevotes84_test_data)
```

## Results of naive Bayes Algorithm
 
* Rows: Actual affiliation
* Columns: Predicted affiliation

```{r}
knitr::kable(table(k_housevotes84_test_class,pred))
```

## A Larger Example: Is a Mushroom Poisenous or Edible?

Determining whether a certain mushroom is edible or poisenous given a set of features that can be easily determined is a problem of high practical importance.

We have a "Mushroom Data Set" from the UCI Machine Learning Repository (<https://archive.ics.uci.edu/ml/datasets/mushroom)>. This original dataset has been split up in training and testing data in the R package `xgboost`. For convenience, this dataset is in slightly modified form part of the `krdata` companion R package of this course.

**Agaricus campestris** (<https://en.wikipedia.org/wiki/Agaricus>)
```{r echo=FALSE}
knitr::include_graphics("440px-Agaricus_campestris.jpg")
```

## Higher Mushrooms

```{r echo=FALSE}
knitr::include_graphics("the-mushroom-rundown.png")
```

from: <https://www.fix.com/blog/growing-mushrooms-at-home/>

##  Mushroom Dataset: Original Data

Let us take a quick look at the original data set from the UCI Machine Learning Repository:
The first column of the original data contains the class label of a mushroom: (classes: edible=e, poisonous=p), subsequent columns contain encoded features:

```
p,x,s,n,t,p,f,c,n,k,e,e,s,s,w,w,p,w,o,p,k,s,u
e,x,s,y,t,a,f,c,b,k,e,c,s,s,w,w,p,w,o,p,n,n,g
e,b,s,w,t,l,f,c,b,n,e,c,s,s,w,w,p,w,o,p,n,n,m
p,x,y,w,t,p,f,c,n,n,e,e,s,s,w,w,p,w,o,p,k,s,u
e,x,s,g,f,n,f,w,b,k,t,e,s,s,w,w,p,w,o,e,n,a,g
e,x,y,y,t,a,f,c,b,n,e,c,s,s,w,w,p,w,o,p,k,n,g
...
```

##  Mushroom Dataset: Original Data

```
     1. cap-shape:                bell=b,conical=c,convex=x,flat=f,
                                  knobbed=k,sunken=s
     2. cap-surface:              fibrous=f,grooves=g,scaly=y,smooth=s
     3. cap-color:                brown=n,buff=b,cinnamon=c,gray=g,green=r,
                                  pink=p,purple=u,red=e,white=w,yellow=y
     4. bruises?:                 bruises=t,no=f
     5. odor:                     almond=a,anise=l,creosote=c,fishy=y,foul=f,
                                  musty=m,none=n,pungent=p,spicy=s
     6. gill-attachment:          attached=a,descending=d,free=f,notched=n
     7. gill-spacing:             close=c,crowded=w,distant=d
     8. gill-size:                broad=b,narrow=n
     9. gill-color:               black=k,brown=n,buff=b,chocolate=h,gray=g,
                                  green=r,orange=o,pink=p,purple=u,red=e,
                                  white=w,yellow=y
    10. stalk-shape:              enlarging=e,tapering=t
    11. stalk-root:               bulbous=b,club=c,cup=u,equal=e,
                                  rhizomorphs=z,rooted=r,missing=?
    12. stalk-surface-above-ring: fibrous=f,scaly=y,silky=k,smooth=s
    13. stalk-surface-below-ring: fibrous=f,scaly=y,silky=k,smooth=s
    14. stalk-color-above-ring:   brown=n,buff=b,cinnamon=c,gray=g,orange=o,
                                  pink=p,red=e,white=w,yellow=y
    15. stalk-color-below-ring:   brown=n,buff=b,cinnamon=c,gray=g,orange=o,
                                  pink=p,red=e,white=w,yellow=y
    16. veil-type:                partial=p,universal=u
    17. veil-color:               brown=n,orange=o,white=w,yellow=y
    18. ring-number:              none=n,one=o,two=t
    19. ring-type:                cobwebby=c,evanescent=e,flaring=f,large=l,
                                  none=n,pendant=p,sheathing=s,zone=z
    20. spore-print-color:        black=k,brown=n,buff=b,chocolate=h,green=r,
                                  orange=o,purple=u,white=w,yellow=y
    21. population:               abundant=a,clustered=c,numerous=n,
                                  scattered=s,several=v,solitary=y
    22. habitat:                  grasses=g,leaves=l,meadows=m,paths=p,
                                  urban=u,waste=w,woods=d
```

## Mushroom Dataset: Example Data

The data set consists of `r ncol(k_agaricus_train_data)` features that are either 1 or 0:

```{r}
# devtools::install_bitbucket("eckartb/krdata")
data(k_agaricus, package="krdata") # in-house R package available via bitbucket
ls(pattern="k_agar.*")
```

As you can see, with the command `data(k_agaricus)` 4 datasets are made available corresponding to the data and labels for testing and training.

## Mushroom Dataset: Example Data 

```{r}
head(cbind(k_agaricus_train_data[,1:7], Poisenous=k_agaricus_train_label))
```

Notice how the factor variable cap-shape with 6 possible values (bell=b, conical=c, convex=x, flat=f, knobbed=k, sunken=s) is represented as 6 columns that contains 1 or 0 corresponding to the presence or absence of a feature value, respectively.
This representation is commonly use in machine learning, it is called **one-hot encoding**.

## Using `naiveBayes`

```{r}
nbmodel <- naiveBayes(k_agaricus_train_data, k_agaricus_train_label)
nbmodel
```

## Predictions using `naiveBayes`

```{r}
nbpred <- predict(nbmodel, k_agaricus_test_data)
table(k_agaricus_test_label, nbpred)
```

In other words, the model leads for the test data to 718 true positives, 787 true negatives, 48 false positives and 58 false negatives.
(Again, the convention of this particular dataset is that "positive" corresponds to poisenous and "negative" corresponds to edible mushrooms).

Exercise:

* What is the accuracy of the approach?
* What is the sensitivity of the approach?
* What is the specificity of the approach?
* What result to do you obtain if you use instead of k-nearest neigbor method (function `knn` from R package "class")? Why might the k-NN method work be more accurate?

## Example: Continuous Data

* The shown Bayes Rule formalism is in a strict sense only applicable to discrete data. * However, continuous data can be converted into "bins" (similar to a histogram) in order to convert is to discrete data.
* The function `naiveBayes` performs this conversion internally so we can use it with continuous data.
* We use a subset of iris data set that consists of only 2 (instead originally 4) features and that consists of 2 species (Iris versicolor and Iris virginica)

```{r}
data(k_iris2d,package="krdata")
```

## Training Model

```{r}
m <- naiveBayes(k_iris2d_train_data,k_iris2d_train_out)
```

## Evaluating Model

```{r}
knitr::kable(table(predict(m, k_iris2d_test_data), k_iris2d_test_out))
```

## Summary

* Bayesian formalism estimates probability of an outcome being true given the data
* We typically have the reverse: for a given outcome, we have probabilities for data
* Bayesian formalism helps to convert what we have (probabilities of data) into what we need (probabilities of outcomes)
* Naive Bayesian methods were initially intended to make predictions for categorical data
* Continuous data can be converted to categorical data via binning
* relatively easy-to-use function `naiveBayes` in R package `e1071`

