---
title: "Preparing Data for Machine Learning"
author: "Eckart Bindewald"
date: "1/23/2019"
output: ioslides_presentation
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = FALSE)
```

## Data Preparation

* Preparing data for machine learning projects is an essential skill
* Often more time-consuming than actual "learning" part of project!
* Requires solid data manipulation skill in language (R)

## General Best Practices

* Make a written note for when you obtained what data
* Communicate with experts provided the data, take notes alongside

## Raw Data: Excel

* Often data is provided in form of Microsoft Excel data
* An MS Excel file with several sheets is often too complex for our purposes
* Create safty copy
* Standard approach: simplify so that each "sheet" corresponds to one table
* Export each relevant sheet in comma-separated-value (CSV) format
* Import data with command `read.csv` or `readr::read_csv` 
* In this class we avoid factors (argument `stringsAsFactors=FALSE`)

## General

* Check that column names are as desired (function `colnames`)
* Potentially remove bad data such as table rows containing missing values (NA, use function `na.omit`)
* Potentially convert column values to desired data type
* Shuffle rows using the `sample` function (example: `iris[nrow(iris),]`)
* Decide on a fraction of testing versus training data. Typical values for testing set sizes are 10%, 20% or 40%. In this class we often use 20% for testing and 80% for training machine learning models.
* Separate each of the two files into a data frame corresponding to the input data and a vector corresponding to the target class labels that we want to predict. This should lead to 4 data structures (data and labels for testing and training set)

## Conventions used in this class

* Use a fraction of testing data of 20% (so 80% for training)
* Avoid factors.
* For 2-class categorical data: Use Logical flags or 0/1 but not "yes"/"no" etc
* Our convention: Use endings `_train_data`, `_train_label`, `_test_data`,`_test_label`

## Example Code: Iris Dataset

```
  train_frac=0.6 # fraction of training data
  ir <- iris[iris$Species != "setosa", ]
  ir$Species <- as.character(ir$Species) # convert factor to character string
  sample_ids <- sample(nrow(ir))
  alld <- all[, c("Petal.Length", "Petal.Width")]
  allc <- all[, "Species"]
  n <- nrow(all)
  ntrain <- round(train_frac*n)
  ntest <- n - ntrain
  train_ids <- 1:ntrain
  test_ids <- (ntrain+1):n
  iris_train_data <- alld[train_ids, ]
  iris_test_data <- alld[test_ids, ]
  iris_train_label <- allc[train_ids]
  iris_test_label <- allc[test_ids]
```

## Excercise

* Create 4 different data structures corresponding to the `iris` dataset
* 20% of the data should be used for testing
* Use columns `Sepal.Length` and `Sepal.Width` as features
* Check if an NA values
* Save files with `save(obj1,obj2,obj3,obj4,file="irissepal.rda")`
* Start a new R session. Load data with `load("irissepal.rda")
* Verify that you obtained the four data structures you created earlier.


