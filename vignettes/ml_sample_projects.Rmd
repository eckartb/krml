---
title: "Machine Learning Sample Project"
author: "Eckart Bindewald"
date: "2/19/2019"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
library(krdata)
data(k_agaricus, package="krdata")
```

# Abstract

Distinguishing edible from poisenous mushroom is a critical skill for the recreational use of mushroom as food. Here a dataset of `r nrow(k_agaricus_train_data)+nrow(k_agaricus_test_data)` mushroom that are annotated as edible or poisenous is analyzed using a machine learning approach. Using a set of `r ncol(k_agaricus_train_data)` attributes, 3 different machline learning approaches are used to predict a binary edible vs poisenous annotation.
We find that edible mushroom can be reliably identified using only a set of categorical attributes.

# Data Collection

The original dataset was obtained from the UCI machine learning repository. The URL of the data set is <https://archive.ics.uci.edu/ml/datasets/mushroom>. A further processed form of the data set was utilized from Kaggle <https://www.kaggle.com/uciml/mushroom-classification>.

# Data Preparation

The data preparation provided by Kaggle corresponds to converting categorical multi-class labels into one-hot encoded binary features. This data set was download and devided into 20% testing and 80% training data, containing `r nrow(k_agaricus_test_data)` and `r nrow(k_agaricus_train_data)` cases, respectively. Also the attributes used for learning were separated from the target vaues to be predicted. This data was obtained from the R package `krdata`. It is available using the R command `data(k_agaricus)`. [YOU SHOULD CREATE A COPY OF THE DATA IN THE R PACKAGE YOU ARE CREATING FOR THE PROJECT]

# Data Exploration

[TRY TO ADD A FIGURE WITH A DATA VISUALIZATION]

# Training The Models

The `naiveBayes` function from the R package `e1071` was used in order to apply the naive Bayes approach for classifying the input data. The performance of the approach was evaluated using `laplace` parameter being set to 0 or 1.

The `C5.0` function from the R package `C50` was used as an example of a decision tree approach for machine learning.

The function `knn` from the R package `class` was used for using a k-nearest neighbor approach in order to classify the input data. Different values of parameter `k` were tried out [WHICH ONES?].

Applying the 3 approaches with default parameters, leads to the following results:

```{r include=TRUE}
# while developing, set include=TRUE so you can see the code
# library(C50)
# library(class)
# library(e1071)

# YOUR ANALYSIS GOES HERE!!

```

# Evaluating the Models

```{r}
# CODE FOR COMPUTING CONFUSION MATRIX, SPECIFICITY, ACCURACY ETC.
```
[DO NOT MANUALLY EDIT THIS TABLE. INSTEAD CREATE A DATA FRAME, AND DISPLAY IT WITH `knitr::kable`]

  Method | TP | TN | FP | FN | Sensitivity | Specificity | J-Score | MCC| 
|--------|----|----|----|----|-------------|-------------|---------|----|
| k-NN   |  12| 34 | 45 | 67 | 0.1234      | 0.9876      | 0.777   |0.88|
| C5.0   |  12| 34 | 45 | 67 | 0.1234      | 0.9876      |  TBD    |TBD |
|n.Bayes |  12| 34 | 45 | 67 | 0.1234      | 0.9876      |  TBD    |TBD |

# Improving Model Performance

```{r}
# YOUR CODE GOES HERE
```

Different parameters were tried out for each method. Also, the approaches were applied with normalized (using the `scale` function) as well as with the original data. Using improved parameters, we find the following improved results:

  Method | TP | TN | FP | FN | Sensitivity | Specificity |Accuracy  | MCC| 
|--------|----|----|----|----|-------------|-------------|----------|----|
| k-NN   |  12| 34 | 45 | 67 | 0.234       | 0.9976      | 0.789    |0.98|
| C5.0   |  11| 34 | 46 | 67 | 0.124       | 0.9922      |  TBD     |TBD |
|n.Bayes |  10| 34 | 47 | 67 | 0.121       | 0.9945      |  TBD     |TBD |

One can see from Table 2 that the method XYZ outperforms the other two approach both in terms of accuracy as well as Matthews Correlation Coefficient.

# Conclusion

Analyzing the mushroom dataset, the found approach XYZ outperformed approaches Y and Z. The found prediction accuracy and Matthews Correlation Coefficient was 0.789 and 0.98 respectively. This shows that that machine learning can be used for distinguishing edible from poisenous mushroom, however not with infallible accuracy.

