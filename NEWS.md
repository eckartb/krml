# krml 0.2.0

* Added function iris2d_generate for reproducible obtaining pre-processed data that can be used for machine learning projects.
* Added slides corresponding to model evaluation
* Added slides corresponding to k-Nearest Neighbor approach

# krml 0.2.1

* Added slides corresponding to naive Bayesian approach (file `naivebayes.Rmd`)


